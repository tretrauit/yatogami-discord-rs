use std::env;

use serenity::{
    async_trait,
    client::{Client, Context, EventHandler},
    framework::{
        StandardFramework,
        standard::{
            macros::{command, group},
            CommandResult
        }
    },
    model::channel::{Message, Embed, EmbedFooter, EmbedImage, EmbedThumbnail, EmbedVideo, EmbedField},
    utils::{
        Colour
    }
};

#[group]
#[commands(ping)]
#[commands(avatar)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("y!"))
        .group(&GENERAL_GROUP);

    // Login with a bot token from the environment
    let token = env::var("DISCORD_TOKEN").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    // start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn avatar(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, format!("awdă {}!", msg.author.avatar_url()
    .as_ref()
    .map_or("not found", String::as_str))).await?;
    Ok(())
}

#[command]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, format!("Hello, {}"), msg.author.nick_in({}, msg.guild_id)).await?;
    Ok(())
}